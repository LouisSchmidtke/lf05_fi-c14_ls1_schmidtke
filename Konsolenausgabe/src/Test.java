
public class Test {

public static void main(String[] args) {
		
		//Aufgabe 1
		String name = "Louis Schmidtke";
		System.out.println("Guten Tag" + name + "!\n");
		System.out.println("Willkommen in der Veranstaltung Strukturierte Programmierung.");
		// println() gibt Anführungszeichen sowie Verkettungsoperatoren mit einem 
		// Leerzeichen, währendessen print() das nicht macht.
		System.out.println("----------\n");
		System.out.println("Test Test Test\b");
		System.out.println("Test Test Test\f");
		System.out.println("Test Test Test\"");
		System.out.println("Test Test Test\t hh");
		System.out.println("Test Test Test\r hh");
		System.out.println("Test Test Test\'");
		System.out.println("Test Test Test\\ ");
		System.out.println("----------\n");
		
		// Aufgabe 2
		String d = "***************************************";
		System.out.printf("%10.1s\n", d);
		System.out.printf("%11.3s\n", d);
		System.out.printf("%12.5s\n", d);
		System.out.printf("%13.7s\n", d);
		System.out.printf("%14.9s\n", d);
		System.out.printf("%15.11s\n", d);
		System.out.printf("%16.13s\n", d);
		System.out.printf("%11.3s\n", d);
		System.out.printf("%11.3s\n", d);
		
		System.out.println("----------\n");
		
		// Aufgabe 3
		
		double i1 = 22.4234234; 
		double i2 = 111.2222; 
		double i3 = 4.0;  
		double i4 = 1000000.551; 
		double i5 = 97.34; 
		
		System.out.printf( "%.2f\n", i1 );
		System.out.printf( "%.2f\n", i2 );
		System.out.printf( "%.2f\n", i3 );
		System.out.printf( "%.2f\n", i4 );
		System.out.printf( "%.2f\n", i5 );
		
	}

}
