import java.util.Scanner;

class Fahrkartenautomat_3 {
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag = 0;
		double eingezahlterGesamtbetrag = 0;
		double eingeworfeneM�nze = 0;
		double r�ckgabebetrag = 0;

		zuZahlenderBetrag = fahrkartenbestellungErfassen(zuZahlenderBetrag);
		eingezahlterGesamtbetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, eingeworfeneM�nze, zuZahlenderBetrag);
		r�ckgabebetrag = fahrkartenAusgeben( r�ckgabebetrag,  zuZahlenderBetrag,  eingezahlterGesamtbetrag);

		// System.out.print("Zu zahlender Betrag (EURO): ");
		// zuZahlenderBetrag = tastatur.nextDouble();

		// Geldeinwurf
		// -----------
		// eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		if (r�ckgabebetrag > eingezahlterGesamtbetrag) {
			System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f Euro ", (r�ckgabebetrag));
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			if (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			else if (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			else if (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			else if (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			else if (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			else if (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
		System.out.print(r�ckgabebetrag);
	}

	// System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	// "vor Fahrtantritt entwerten zu lassen!\n"+
	// "Wir w�nschen Ihnen eine gute Fahrt.");

	static double fahrkartenbestellungErfassen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);

		System.out.print("Zu zahlender Betrag (in EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		return zuZahlenderBetrag;

	}

	static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
			double eingeworfeneM�nze) {

		// Scanner tastatur = new Scanner(System.in);

		eingezahlterGesamtbetrag = 0.0;
		return eingezahlterGesamtbetrag;
	}

	static double fahrkartenAusgeben(double r�ckgabebetrag, double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {

		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		

		

		return r�ckgabebetrag;

	}
}