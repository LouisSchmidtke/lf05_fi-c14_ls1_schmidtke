
public class Konfiguration {

	public static void main(String[] args) {
		
		String name;
		final double maximum = 100.00;
		char sprachModul = 'd';
		byte PRUEFNR = 4;
		double patrone = 46.24; 
		int cent;
		double fuellstand;
		int summe;
		boolean statusCheck;
		String bezeichnung = "Q2021_FAB_A";
		String typ = "Automat AVR";
		name = typ + " " + bezeichnung;
		fuellstand = maximum - patrone;
		
		System.out.println("F�llstand Patrone: " + fuellstand + " %");
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Name: " + name);
		
		int muenzenEuro = 130;  
		int muenzenCent = 1280;
		 summe = muenzenCent + muenzenEuro * 100; 
		int euro = summe / 100;	
		
		cent = summe % 100;
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Summe Euro: " + euro +  " Euro");
		statusCheck = (euro <= 150)&& (fuellstand >= 50.00) && (euro >= 50)&& (cent != 0)
		&& (sprachModul == 'd')&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
		System.out.println("Status: " + statusCheck);

	}

}
