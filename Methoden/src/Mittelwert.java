public class Mittelwert {

	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double mittelwert;
      mittelwert = verarbeiten(x,y);

      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, mittelwert);
   }
   
   static double verarbeiten(double x, double y){
	      double mittelwert = (x + y) / 2.0;
	      return mittelwert;
	      
	      }
   
}
