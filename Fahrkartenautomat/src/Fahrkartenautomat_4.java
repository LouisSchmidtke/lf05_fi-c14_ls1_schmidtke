import java.util.Scanner;

public class Fahrkartenautomat_4 {

	public static void main(String[] args) {

		
		Scanner tastatur = new Scanner(System.in);
		
		int fahrkartenAnzahl = 0;
		double zuZahlenderBetrag = 0; 
		double rueckgabebetrag = 0;
		double eingezahlterGesamtbetrag = 0;
		double eingeworfeneMuenze = 0;
		
		
		zuZahlenderBetrag = (fahrkartenbestellungErfassen(fahrkartenAnzahl) * 3.50) ;
		eingezahlterGesamtbetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, eingeworfeneMuenze);
		fahrkartenAusgeben();
		rueckgeldAusgeben (rueckgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag);

		
	}
 
	static int fahrkartenbestellungErfassen (int fahrkartenAnzahl) {
		
		Scanner tastatur = new Scanner(System.in);

		System.out.printf("Ein Ticket kostet 3,50 Euro \n" + "Wie viele Tickets wollen Sie kaufen? \n" + "(Bitte nur in ganzen Zahlen, mindestens ein Ticket und maximal 10.) \n\n");
		System.out.print("Ticketanzahl: ");
		fahrkartenAnzahl = tastatur.nextInt();
		
		if (fahrkartenAnzahl > 10) {
			
			fahrkartenAnzahl = 1;
			System.out.print("Sie k�nnen maximal 10 Tickets gelichzeitig kaufen!\nDer Vorgang wird nun mit einem Ticket fortgef�hrt.\n\n");
			
		}
			
		if (fahrkartenAnzahl < 1) {
			
			fahrkartenAnzahl = 1;
			System.out.print("Sie m�ssen mindestens ein Ticket kaufen!\nDer Vorgang wird nun mit einem Ticket fortgesetzt.\n\n");
			
		}
			
		
		
		return fahrkartenAnzahl;
	
	}
	
	static double fahrkartenBezahlen (double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double eingeworfeneMuenze) {
		
		Scanner tastatur = new Scanner(System.in);
		
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		
		
		return eingezahlterGesamtbetrag;
	}
	
	static void fahrkartenAusgeben() {
		
		 System.out.println("\nVergessen Sie nicht, den Fahrschein "+
		 "vor Fahrtantritt entwerten zu lassen!\n"+
		 "Wir wuenschen Ihnen eine gute Fahrt.\n");
		 
	}

	static double rueckgeldAusgeben (double rueckgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; 
		
		if (rueckgabebetrag > 0) {
			System.out.printf("Der Rueckgabebetrag in H�he von " + "%.2f Euro ", (rueckgabebetrag));
			System.out.println("wird in folgenden Muenzen ausgezahlt:");
			
			
			while (rueckgabebetrag > 0) {

				if (rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
				{
					System.out.println("2 EURO");
					rueckgabebetrag -= 2.0;
				} else if (rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
				{
					System.out.println("1 EURO");
					rueckgabebetrag -= 1.0;
				} else if (rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
				{
					System.out.println("50 CENT");
					rueckgabebetrag -= 0.5;
				} else if (rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
				{
					System.out.println("20 CENT");
					rueckgabebetrag -= 0.2;
				} else if (rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
				{
					System.out.println("10 CENT");
					rueckgabebetrag -= 0.1;
				} else if (rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
				{
					System.out.println("5 CENT");
					rueckgabebetrag -= 0.05;
				} 
					else if (rueckgabebetrag >= 0.02)// 2 CENT-Muenzen
				{
					System.out.println("2 CENT");
					rueckgabebetrag -= 0.02;
				} else if (rueckgabebetrag >= 0.01)// 1 CENT-Muenzen
				{
					System.out.println("1 CENT");
					rueckgabebetrag -= 0.01;
				}
			}
		}
		
		return rueckgabebetrag;
		
	}
	
	
}
