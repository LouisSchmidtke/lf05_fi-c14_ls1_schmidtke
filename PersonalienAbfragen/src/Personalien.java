import java.util.Scanner;
public class Personalien {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Guten Tag, bitte geben Sie ihren Namen und ihr Alter an.");
		
		System.out.print("\n\n\nBitte geben Sie ihren Namen an.");
		String name = myScanner.nextLine();
		
		System.out.print("Bitte geben Sie ihr Alter an.");
		int alter = myScanner.nextInt();
			
		System.out.print("\n\n\n Name:");
		System.out.print(name);
		
		System.out.print("\n\n\n Alter:");
		System.out.print(alter);
	
		myScanner.close();
	}

}